using Microsoft.AspNetCore.Mvc;
using SkiaSharp;

namespace IMGControler
{

    [ApiController]
    [Route("[controller]")]
    public class IMGControlerController : ControllerBase
    {


        [HttpPost]
        public IActionResult Post(IFormFile image)
        {
            try
            {
                // Validar se existe algum arquivo
                if (image == null) return null;



                // Salva imagem original enviada pelo usuario
                using (var stream = new FileStream(Path.Combine("imgs/orig", image.FileName), FileMode.Create))
                {
                    image.CopyTo(stream);
                }

                var guid = Guid.NewGuid();

                // Salva imagem no formato webp
                using (var webpFileStream = new FileStream(Path.Combine("imgs/webp", guid + ".webp"), FileMode.Create))
                {
                    SKBitmap.Decode(image.OpenReadStream()).Encode(SKEncodedImageFormat.Webp, 100).SaveTo(webpFileStream);

                }
                // string baseUri = $"{Request.Scheme}://{Request.Host}:{Request.Host.Port ?? 80}";
                // string urlcompleta = baseUri + Path.Combine(baseUri.ToString(), "/imgs/webp", guid + ".webp");
                return Ok($"{Request.Scheme}://{Request.Host}:{Request.Host.Port ?? 80}" + "/imgs/webp/" + guid + ".webp");



            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);


            }



        }
    }
}